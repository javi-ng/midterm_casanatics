CSCI 40 - C

GROUP 5: Casanatics
Alvarez, Angelo Joaquin B., ID # 210295
Limbaga, Gabriel S., ID # 213484
Ng, John Aidan Vincent M., ID # 214221
Reyes, Justin Carlo J., ID # 214969
Tan, Andre Dalwin C., ID # 215736

Final Project: Widget v2

Assignment:
Alvarez: Calendar
Limbaga: Assignments
Ng: Announcement Board
Reyes: Dashboard
Tan: Forum

Submission Date: 05/15/2023

We truthfully completed this project (this project being Final Project: Widget v2). There are no lies because it is true.

References:
Date-time conversion to timezone in views: https://stackoverflow.com/questions/35072711/django-datetime-field-convert-to-timezone-in-view
Automatically computed fields: https://stackoverflow.com/questions/17682567/how-to-add-a-calculated-field-to-a-django-model
Automatically computed fields(1): https://www.youtube.com/watch?v=IkwG2MuUdRo
Field reference: https://docs.djangoproject.com/en/4.0/ref/models/fields/


(sgd) Angelo Joaquin B. Alvarez, 05/14/2023
(Sgd) Gabriel S. Limbaga, 05/14/2023
(sgd) John Aidan Vincent M. Ng, 05/14/2023
(sgd) Justin Carlo J. Reyes, 05/14/2023
(sgd) Andre Dalwin C. Tan, 05/14/2023
