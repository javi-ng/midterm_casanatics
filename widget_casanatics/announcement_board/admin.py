from django.contrib import admin
from .models import Announcement, Reaction


# show reactions in page for announcements
class ReactionInline(admin.TabularInline):
    model = Reaction


# announcement admin panel
class AnnouncementAdmin(admin.ModelAdmin):
    model = Announcement

    # display title, body, datetime, author
    list_display = ("title", "get_author", "pub_datetime", "body")

    # search by title or body
    search_fields = ("title", "body")

    # filter by author
    list_filter = ("author",)

    def get_author(self, obj):
        return obj.author.displayName()

    get_author.short_description = "Author Name"
    get_author.admin_order_field = "author"

    inlines = [ReactionInline]


class ReactionAdmin(admin.ModelAdmin):

    # display reaction name, tally, announcement
    list_display = ("name", "tally", "announcement")

    # search by announcement name
    search_fields = ("name",)

    # filter by announcement
    list_filter = ("announcement__title",)
    #
    # def get_name(self, obj):
    #     return obj.announcement.title
    #
    # # title of column for get_name
    # get_name.short_description = "Announcement Title"
    #
    # # sorting by announcement
    # get_name.admin_order_field = "announcement"


admin.site.register(Announcement, AnnouncementAdmin)
admin.site.register(Reaction, ReactionAdmin)
