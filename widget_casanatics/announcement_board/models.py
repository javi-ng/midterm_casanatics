from django.db import models
from dashboard.models import WidgetUser
from django.urls import reverse


class Announcement(models.Model):
    title = models.CharField(max_length=50)
    body = models.CharField(max_length=500)
    author = models.ForeignKey(WidgetUser, on_delete=models.CASCADE)
    pub_datetime = models.DateTimeField(
        "Date and Time Published",
        auto_now_add=True,
    )

    def __str__(self):
        return self.title
    
    # get corresponding detail page
    def get_absolute_url(self):
        return(reverse('announcement_board:announcementdetail', kwargs={'pk': self.pk}))


class Reaction(models.Model):

    name = models.CharField(max_length=50)
    tally = models.IntegerField(default=0)
    announcement = models.ForeignKey(Announcement, on_delete=models.CASCADE, related_name='reactlist')

    def __str__(self):
        return self.name
