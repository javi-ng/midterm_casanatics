from django.urls import path
from . import views

urlpatterns = [
    # main page listing announcements
    path("", views.announcements, name="announcements"),

    # detail page for each announcement
    path("<int:pk>/details/", views.AnnouncementDetailView.as_view(), name="announcementdetail"),

    # create page
    path("add/", views.AnnouncementCreateView.as_view(), name="announcementadd"),

    # edit page for each announcement
    path("<int:pk>/edit/", views.AnnouncementUpdateView.as_view(), name="announcementupdate"),
]


app_name = "announcement_board"
