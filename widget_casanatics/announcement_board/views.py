from django.http import HttpResponse
from .models import Announcement, Reaction
from datetime import datetime
from django.utils import timezone
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView
from django.shortcuts import render

def announcements(request):

    # use context to pass announcements sorted by pubdate
    context = {
        "announcement_list" : Announcement.objects.all().order_by('-pub_datetime'),
    }

    return render(request, 'announcement_board/announcements.html', context)

class AnnouncementDetailView(DetailView):
    model = Announcement
    template_name = 'announcement_board/announcement-details.html'

class AnnouncementCreateView(CreateView):
    model = Announcement
    template_name = 'announcement_board/announcement-add.html'
    fields = ['title', 'body', 'author']

class AnnouncementUpdateView(UpdateView):
    model = Announcement
    template_name = 'announcement_board/announcement-edit.html'
    fields = ['title', 'body', 'author']