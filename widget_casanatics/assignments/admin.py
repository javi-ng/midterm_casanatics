from django.contrib import admin
from .models import Assignment, Course
# Register your models here.

class AssignmentAdmin(admin.ModelAdmin):
    model = Assignment
    list_display = ("name", "description", "course", "perfect_score", "passing_score")

class CourseAdmin(admin.ModelAdmin):
    model = Course
    list_display = ("code", "title", "section")

admin.site.register(Assignment, AssignmentAdmin)
admin.site.register(Course, CourseAdmin)