from django.db import models
from django.urls import reverse

class Course(models.Model):
    code = models.CharField(max_length=10)
    title = models.CharField(max_length=50)
    section = models.CharField(max_length=3)

    def __str__(self):
        return self.code + " " + self.title + " - " + self.section


class Assignment(models.Model):
    name = models.CharField(max_length=100)
    description = models.TextField(blank=True)
    course = models.ForeignKey(Course, on_delete=models.CASCADE)
    perfect_score = models.IntegerField(default=0)

    @property
    def passing_score(self) -> int:
        if self.perfect_score != None:
            return round(self.perfect_score * 0.60)

    def __str__(self):
        return self.name
    
    def get_absolute_url(self):
        return reverse('assignments:assignment-details', kwargs={'pk': self.pk})
