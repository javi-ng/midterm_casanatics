from django.http import HttpResponse
from .models import Assignment, Course
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView
from django.shortcuts import render
from django.template import loader

def assignments(request):
    assignments = Assignment.objects.all()
    context = {'assignments': assignments,}

    return render(request, 'assignments/assignments.html', context)

class AssignmentDetailView(DetailView):
    model = Assignment  
    template_name = 'assignments/assignment-details.html'

class AssignmentCreateView(CreateView):
    model = Assignment  
    fields = '__all__'
    template_name = 'assignments/assignment-add.html'

class AssignmentUpdateView(UpdateView):
    model = Assignment 
    fields = '__all__' 
    template_name = 'assignments/assignment-edit.html'
