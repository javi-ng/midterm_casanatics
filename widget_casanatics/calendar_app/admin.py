from django.contrib import admin
from .models import Location, Event


class LocationAdmin(admin.ModelAdmin):
    model = Location
    list_display = (
        "venue",
        "mode",
    )

    search_fields = (
        "venue",
        "mode",
    )


class EventAdmin(admin.ModelAdmin):
    model = Event
    list_display = (
        "activity",
        "target_datetime",
        "estimated_hours",
        "location",
        "course",
    )

    search_fields = (
        "activity",
        "location",
        "course",
    )


admin.site.register(Location, LocationAdmin)
admin.site.register(Event, EventAdmin)
