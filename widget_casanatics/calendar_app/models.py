from django.db import models
from assignments.models import Course
from django.urls import reverse

# Create your models here.
class Location(models.Model):
    LOCATION_CHOICES = [
        ("ONSITE", "Onsite"),
        ("ONLINE", "Online"),
        ("HYBRID", "Hybrid"),
    ]

    mode = models.CharField(max_length=6, choices=LOCATION_CHOICES, default="ONSITE")
    venue = models.TextField(max_length=255)

    def __str__(self):
        return self.mode + " - " + self.venue.replace('\r\n', ' or ')
    
    def getvenue(self):
        return self.venue.replace('\r\n', ' or ')




class Event(models.Model):
    target_datetime = models.DateTimeField(
        "Target Date and Time",
    )
    activity = models.CharField(max_length=50)
    estimated_hours = models.FloatField()
    location = models.ForeignKey(Location, on_delete=models.CASCADE)
    course = models.ForeignKey(Course, on_delete=models.CASCADE)

    def __str__(self):
        return self.activity
    
    def get_absolute_url(self):
        return reverse('calendar_app:events-details', kwargs={'pk': self.pk})
    
