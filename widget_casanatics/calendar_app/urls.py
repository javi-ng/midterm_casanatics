# <appname>/urls.py
from django.urls import path
from .views import calendar, EventDetailView, EventCreateView, EventUpdateView

urlpatterns = [
    path('', calendar, name='calendar'),
    path('events/<int:pk>/details/', EventDetailView.as_view(), name='events-details'),
    path('events/add/', EventCreateView.as_view(), name='add-event'),
    path('events/<int:pk>/edit/', EventUpdateView.as_view(), name='edit-event'),
]

app_name = "calendar_app"