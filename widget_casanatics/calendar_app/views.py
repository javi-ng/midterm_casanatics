from django.shortcuts import render
from django.http import HttpResponse
from .models import Event
from django import forms
from django.utils import timezone
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView

# Create your views here.

def calendar(request):
    context = {}

    context["object_list"] = Event.objects.all()
    return render(request, 'calendar_app/calendar.html', context)

class EventDetailView(DetailView):
    template_name = 'calendar_app/event-details.html'
    model = Event

class EventCreateView(CreateView):
    template_name = 'calendar_app/event-add.html'
    model = Event
    fields = ['activity', 'target_datetime', 'estimated_hours', 'location', 'course']

class EventUpdateView(UpdateView):
    template_name = 'calendar_app/event-edit.html'
    model = Event
    fields = ['activity', 'target_datetime', 'estimated_hours', 'location', 'course']