from django.contrib import admin
from .models import Department, WidgetUser


class WidgetUserInline(admin.TabularInline):
    model = WidgetUser


class DepartmentAdmin(admin.ModelAdmin):
    model = Department
    list_display = (
        "dept_name",
        "home_unit",
    )
    search_fields = (
        "dept_name",
        "home_unit",
    )
    list_filter = ("home_unit",)
    inlines = [WidgetUserInline]


class WidgetUserAdmin(admin.ModelAdmin):
    model = WidgetUser
    list_display = (
        "last_name",
        "first_name",
        "middle_name",
        "department",
    )
    search_fields = (
        "first_name",
        "middle_name",
        "last_name",
        "department",
    )


admin.site.register(WidgetUser, WidgetUserAdmin)
admin.site.register(Department, DepartmentAdmin)
