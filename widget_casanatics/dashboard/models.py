from django.db import models
from django.urls import reverse


class Department(models.Model):
    dept_name = models.CharField(max_length=255)
    home_unit = models.CharField(max_length=255)

    def __str__(self):
        return f"{self.dept_name}, {self.home_unit}"


class WidgetUser(models.Model):
    first_name = models.CharField(max_length=255)
    middle_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    department = models.ForeignKey(
        Department,
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return f"{self.last_name}, {self.first_name} {self.middle_name}"

    def displayName(self):
        return f"{self.first_name} {self.last_name}"

    def get_absolute_url(self):
        return reverse(
            "widgetusers:user-detail",
            kwargs={"pk": self.pk},
        )
