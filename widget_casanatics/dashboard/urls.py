from django.urls import path
from . import views


urlpatterns = [
    path("", views.dashboard, name="dashboard"),
    path(
        "<int:pk>/details/",
        views.WidgetUserDetailView.as_view(),
        name="user-detail",
    ),
    path(
        "add/",
        views.WidgetUserCreateView.as_view(),
        name="user-add",
    ),
    path(
        "<int:pk>/edit/",
        views.WidgetUserUpdateView.as_view(),
        name="user-edit",
    ),
]


app_name = "dashboard"
