# from django.http import HttpResponse
from django.views.generic.detail import DetailView
from django.views.generic.edit import UpdateView, CreateView
from .models import WidgetUser
from django.shortcuts import render


def dashboard(request):
    users = WidgetUser.objects.all()
    context = {"users": users}
    return render(
        request,
        "dashboard/dashboard.html",
        context,
    )


class WidgetUserDetailView(DetailView):
    model = WidgetUser
    fields = "__all__"
    template_name = "dashboard/widgetuser-details.html"


class WidgetUserUpdateView(UpdateView):
    model = WidgetUser
    fields = "__all__"
    template_name = "dashboard/widgetuser-edit.html"


class WidgetUserCreateView(CreateView):
    model = WidgetUser
    fields = "__all__"
    template_name = "dashboard/widgetuser-add.html"
