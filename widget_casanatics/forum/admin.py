from django.contrib import admin
from .models import ForumPost, Reply


class ReplyInline(admin.TabularInline):
    model = Reply


class ForumPostAdmin(admin.ModelAdmin):
    model = ForumPost

    list_display = ("title", "author", "body", "pub_datetime")


class ReplyAdmin(admin.ModelAdmin):
    model = Reply

    list_display = ("forum_post", "author", "body", "pub_datetime")


admin.site.register(ForumPost, ForumPostAdmin)
admin.site.register(Reply, ReplyAdmin)
