from django.db import models
from dashboard.models import WidgetUser
from django.urls import reverse


class ForumPost(models.Model):
    title = models.CharField(max_length=50)
    body = models.CharField(max_length=1000)
    author = models.ForeignKey(WidgetUser, on_delete=models.CASCADE)
    pub_datetime = models.DateTimeField("Published Date and Time", auto_now_add=True)

    def __str__(self):
        return self.title
    
    def get_absolute_url(self):
        return reverse('forum:forumpostdetails', kwargs={'pk': self.pk})



class Reply(models.Model):
    forum_post = models.ForeignKey(ForumPost, on_delete=models.CASCADE, related_name="replies")
    body = models.CharField(max_length=1000)
    author = models.ForeignKey(WidgetUser, on_delete=models.CASCADE)
    pub_datetime = models.DateTimeField("Published Date and Time", auto_now_add=True)

    def __str__(self):
        return self.body
