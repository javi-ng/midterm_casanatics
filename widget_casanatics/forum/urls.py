from django.urls import path
from . import views


urlpatterns = [
    path("", views.forum, name="index"),
    path(
        "forumposts/<int:pk>/details/",
        views.ForumDetailView.as_view(),
        name="forumpostdetails",
    ),
    path("forumposts/add/", views.ForumCreateView.as_view(), name="forumpostadd"),
    path(
        "forumposts/<int:pk>/edit/",
        views.ForumUpdateView.as_view(),
        name="forumpostedit",
    ),
]


app_name = "forum"
