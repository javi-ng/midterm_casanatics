from django.http import HttpResponse
from .models import ForumPost
from django.utils import timezone
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView
from django.shortcuts import render



def forum(request):
    context = {
        "forumpost" : ForumPost.objects.all().order_by('pub_datetime'),
    }
    return render(request, 'forum/forum.html', context)

class ForumDetailView(DetailView):
    model = ForumPost
    template_name = 'forum/forumpost-details.html'

class ForumCreateView(CreateView):
    model = ForumPost
    template_name = 'forum/forumpost-add.html'
    fields = ['title', 'body', 'author']

class ForumUpdateView(UpdateView):
    model = ForumPost
    template_name = 'forum/forumpost-edit.html'
    fields = ['title', 'body', 'author']
